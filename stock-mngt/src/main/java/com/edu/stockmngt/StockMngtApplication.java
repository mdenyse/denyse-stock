package com.edu.stockmngt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockMngtApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockMngtApplication.class, args);
	}

}
