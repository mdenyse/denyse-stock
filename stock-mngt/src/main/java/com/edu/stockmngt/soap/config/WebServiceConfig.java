package com.edu.stockmngt.soap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

//Enable Spring Web Services
@EnableWs
//Spring Configuration
@Configuration
public class WebServiceConfig{
    // MessageDispatcherServlet
    // ApplicationContext
    // url -> /ws/*
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/denyse/stock/*");
    }
    // /ws/denyse/suppliers.wsdl
    // supplier-details.xsd
//    @Bean(name = "suppliers")
//    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema stockSchema) {
//        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
//        definition.setPortTypeName("StockPort");
//        definition.setTargetNamespace("http://stockmngt.edu.com/supplier");
//        definition.setLocationUri("/ws/denyse/stock");
//        definition.setSchema(stockSchema);
//        return definition;
//    }
//    @Bean
//    public XsdSchema coursesSchema() {
//        return new SimpleXsdSchema(new ClassPathResource("supplier-details.xsd"));
//    }

    @Bean(name = "items")
    public DefaultWsdl11Definition defaultWsdl11DefinitionItems(XsdSchema itemSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("StockPort");
        definition.setTargetNamespace("http://stockmngt.edu.com/supplier");
        definition.setLocationUri("/ws/denyse/stock");
        definition.setSchema(itemSchema);
        return definition;
    }
    @Bean
    public XsdSchema itemSchema() {
        return new SimpleXsdSchema(new ClassPathResource("item-details.xsd"));
    }
}
