package com.edu.stockmngt.soap.repository;

import com.edu.stockmngt.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemRepository extends JpaRepository<Item,Integer> {
}
