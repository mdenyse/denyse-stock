package com.edu.stockmngt.soap.repository;

import com.edu.stockmngt.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISupplierRepository extends JpaRepository<Supplier, Integer> {
}
