package com.edu.stockmngt.soap.endPoint;
import com.edu.stockmngt.item.*;
import com.edu.stockmngt.soap.bean.Item;
import com.edu.stockmngt.soap.repository.IItemRepository;
import com.edu.stockmngt.soap.repository.IItemRepository;
import com.edu.stockmngt.item.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndpoint {

    @Autowired
    private IItemRepository itemRepository;

    @PayloadRoot(namespace = "http://stockmngt.edu.com/item", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {

        Item item = itemRepository.findById(request.getId()).get();

        GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
        return  itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stockmngt.edu.com/item", localPart = "GetAllItemDetailsRequest")
    @ResponsePayload
    public GetAllItemDetailsResponse findAll(@RequestPayload GetAllItemDetailsRequest request){
        GetAllItemDetailsResponse allCourseDetailsResponse = new GetAllItemDetailsResponse();

        List<Item> items = itemRepository.findAll();
        for (Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
            allCourseDetailsResponse.getItemDetails().add(itemDetailsResponse.getItemDetails());
        }
        return allCourseDetailsResponse;
    }


    @PayloadRoot(namespace = "http://stockmngt.edu.com/item", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        System.out.println("ID: "+request.getItemDetails().getId());
        itemRepository.save(new Item(request.getItemDetails().getId(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getPrice(),
                request.getItemDetails().getName(),
                request.getItemDetails().getSupplier(),
                request.getItemDetails().getStatus()
        ));

        CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
        itemDetailsResponse.setItemDetails(request.getItemDetails());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stockmngt.edu.com/item", localPart = "UpdateItemDetailsRequest")
    @ResponsePayload
    public UpdateItemDetailsResponse update(@RequestPayload UpdateItemDetailsRequest request) {
        UpdateItemDetailsResponse itemDetailsResponse = null;
        Optional<Item> existingItem = this.itemRepository.findById(request.getItemDetails().getId());
        if(existingItem.isEmpty()) {
            itemDetailsResponse = mapItemDetail(null, "Id not found");
        }
        if(existingItem.isPresent()) {

            Item _item = existingItem.get();
            _item.setName(request.getItemDetails().getName());
            _item.setPrice(request.getItemDetails().getPrice());
            _item.setItemCode(request.getItemDetails().getItemCode());
            _item.setSupplier(request.getItemDetails().getSupplier());
            _item.setStatus(request.getItemDetails().getStatus());
            itemRepository.save(_item);
            itemDetailsResponse = mapItemDetail(_item, "Updated successfully");

        }
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stockmngt.edu.com/item", localPart = "DeleteItemDetailsRequest")
    @ResponsePayload
    public DeleteItemDetailsResponse save(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById(request.getId());

        DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }

    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails itemDetails = mapItem(item);

        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        return itemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapItemDetail(Item item, String message) {
        ItemDetails itemDetails = mapItem(item);
        UpdateItemDetailsResponse itemDetailsResponse = new UpdateItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        itemDetailsResponse.setMessage(message);
        return itemDetailsResponse;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setSupplier(item.getSupplier());
        return itemDetails;
    }
}


