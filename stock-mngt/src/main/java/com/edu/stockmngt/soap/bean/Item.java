package com.edu.stockmngt.soap.bean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    private String itemCode;

    private String status;

    private String price;

    private String supplier;

    @Override
    public String toString() {
        return String.format("Course [id=%s, name=%s, description=%s]", id, name, itemCode, status, price, supplier);
    }

}

